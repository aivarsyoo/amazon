# PHP project which implements all the CRUD functionality:
## **Full documentation here:** https://aivarsyoo.gitlab.io/amazon/

> - Registering a user
> - User verification via email and SMS
> - Uploading products to MySQL database
> - Editing, deleting products and user information

## Dockerisation

Easiest way to view the project is by cloning the repository and Dockerising on your local machine. For that you will need to have Docker installed and calling a command.

```
docker-compose up --build
```

### Docker

To have the project work in the background:

```
docker-compose up --build -d
```

To remove the container, images and volumes:

```
docker-compose down --rmi all -v
```

If you are using a virtual machine as Linode for building the project, be cautious if you have the mail sending functionality enabled on the server, because by default ports 25, 465 and 587 may be blocked by the provider and you need to enable them manually. Project will work no matter what, but registering a user will bring an error "could not connect to SMTP host".

## Local deploying

If you are not comfortable working with Docker, for the project to work you will need to have your own database service installed and change a few lines of code.

### Database

On phpMyAdmin just import the database from **mysql_data** directory and change the connection information here **src -> private -> globals.php**. 

I recommend using PHP 7.4 on your Apache server, because there are some inconsistencies with the way how PHP 8.0 treats 404 errors if JS is being used.

### Localhost

Have your localhost configured to port :9000, because the paths for email links are absolute and otherwise need to be changed manually.

### Composer

PHP Mailer relies on Composer dependencies, so it needs to be specified where **autoload.php** is located. On **src -> private -> send_email.php** find this line ```require '../vendor/autoload.php';``` and change it to ```require '../../vendor/autoload.php';```.




