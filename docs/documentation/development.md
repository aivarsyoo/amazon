---
layout: page
title: Development
description: ~
---

This project uses Composer and Node modules for dependencies. It's all about the preference to develop on Docker container or Localhost, it's possible to do both with the configuration I have provided.

## Setting up localhost

First of all, you need to install both dependencies managers.

### Composer

Official installation guide can be found [HERE](https://getcomposer.org/download/).

Composer install:
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('sha384', 'composer-setup.php') === '55ce33d7678c5a611085589f1f3ddf8b3c52d662cd01d4ba75c0ee0459970c2200a51f492d557530c71c15d8dba01eae') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"
```
And then a command to install packages from composer.json or composer.lock (may get outdated)
```
php composer.phar update
```

Remember to check your PHP version and change to PHP 7.4 if needed.

### Node modules

I am using Webpack to optimise JS and CSS styling. The initial styling is done by using SASS.
Most likely you don't need to have a Webpack server provided, so just remove ```serve``` from ```start``` script in **package.json**.

To start developing, use:
```
npm install
```
and then
```
npm run start
```
or
```
npm run build
```

## Dockerisation

To create a Docker container we will be using ```Dockerfile``` and ```docker-compose.yml```. 

There are two ways of how to perceive this Docker container:
1. Development one where local file changes will be displayed without building the container again
- For that we need to comment these lines in `Dockerfile`
```yaml
# COPY ./src /var/www/html/
# COPY ./vendor /var/www/html/vendor
```
- And uncomment these lines in `docker-compose.yml`
```yaml
volumes:
'    - ./vendor:/var/www/html/vendor
'    - ./src:/var/www/html
```
2. Production one where database changes will only mirror what's done inside the container
- &uarr; The opposite actions from what we would do in Development mode &uarr;

### Dockerfile

As the base image it uses `php:7.4-apache`, it allows us to have PHP environment and Apache server running at the same time.

Be aware of this line:
```yaml
RUN chmod -R 777 /var/www/html/
```
It allows write access for everyone, but needs to be restricted when project is pushed to production stage. It's needed, because in the process of sending an email new files are being made to send a message to the user with the right link.

### docker-compose.yml

Three images are made in the Docker container:
1. PHP environment where we'll be able to see the process of an application
2. MySQL database to store all the user data, image information and products on the website. One volume serves as a database import from `mysql_data` directory and other keeps all the changes even when docker is taken down
3. PhpMyAdmin on a different port to explore what is happening in our database

## Writing JS and SASS

For puprose of making it simple, there are one file for JS and one for SASS. In the process of development, we just need to use import statements for both these file types to create a more realistic environment, and as the project grows it wouldn't be suitable to have one file for everything.

Command `npm run dev` in the terminal runs a live development server for both JS and SASS. The compiled files with the current configuration doesn't get displayed in the `dist` directory for better optimisation, but it's possible to change the settings in the `config` directory to overwrite these files once the change happens. The server can be used together with Docker container to develop real time if the volumes are set right in `docker-compose.yml`.

Command `npm run build` allows us to compile the code for the production version. It can also minify images, but that needs to be set up seperately with requiring the correct package. More on that [HERE](https://webpack.js.org/plugins/image-minimizer-webpack-plugin/).


