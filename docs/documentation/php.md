---
layout: page
title: PHP
---

Besides having CRUD functionality in PHP, there are two important implementations made in this project. As we are working with users, first of all, there needs to be some verification to allow other actions on the website e.g. adding products, editing products, deleting products. Until the user has verified his email by clicking on the link he has received, none of these actions will be possible. 

Also, people forget passwords quite easily, that's why every user system needs to have a sections for recovering password. So again, email sending system comes to help by sending another link to the user to change the password. Let's take a look from developing perspective on these things.

## PHP Mailer

[PHP Mailer](https://github.com/PHPMailer/PHPMailer) serves as a PHP package to allow us sending the emails in no time. It gets included by Composer, and all what I had to do was set an email account ready for sending emails.

The file responsible for sending an email is found here **src -> private -> send_email.php**. It defines what email will be used for our procedures. I have commented out one of the solutions, because Google is blocking access to their accounts by just username and password. From 30.05.2022 It's possible to use Gmail as sending service only when XOAuth is set up for every application. In this use case scenario I'm just using one of the emails I can create on my hosting provider.

Afterwards we just need to require this file when the actual function needs to happen, and pass all the variables we have defined before in our general file.

### VPS service

If you're using VPS as Linode for example, then be aware that you should check if outgoing ports 25, 465, 587 are open for your account, because most of the VPS providers block them by default to limit phishing scams. It can be done by contacting support for the provider or somewhere in your settings manually.

## SMS service

When user registers, a text message will be sent to his mobile number to verify that he has registered. I am using [Fat SMS](https://www.fatsms.com/) provided by Santiago. It allows only danish numbers, but for testing purpose it works well enough. Most of these kind of services are asking to buy credits to be able to implement this functionality.