---
layout: page
title: Linting
description: ~
---

## PHP analysis

For testing PHP code this project uses [PHPStan](https://phpstan.org/), what allows us to catch bugs faster than going through the code ourselves. It moves PHP closer to compiled languages in the sense that the correctness of each line of the code can be checked before we run the actual line. Of course, it can't detect everything, but it gives pretty good error messages on noticing something that could be changed. In `phpstan.neon` we can change the setting of excluding specific, ignoring certain errors or changing the level of restrictness. For this project it's the most basic one, but if we really wish to have a code which looks close to what robot would write, then level 9 will serve just well. 

PHPStan is installed using Composer, so we need to be sure that it has updated all the dependencies first and then simply call a command in terminal:
```
vendor/bin/phpstan analyse src --memory-limit=512M
```
I have increased the memory limit, because `src` folder contains plenty of PHP code and the cache needs to be able to go through all the files. But we specify exactly what file or directory to analyse. Once I have repaired all the errors in my PHP files, this message on terminal shows up:
```
 36/36 [▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓▓] 100%                                 
 [OK] No errors   
 ```
 Otherwise detailed errors like these will be displayed:
 ```
  ------ ---------------------------------------------------------- 
  Line   verify-new-email.php                                      
 ------ ---------------------------------------------------------- 
  52     Variable $db might not be defined.                        
  62     Variable $db might not be defined.                        
  101    Function send_400() has no return type specified.         
  101    Function send_400() has parameter $error_message with no  
         type specified.                                           
  108    Function send_200() has no return type specified.         
  108    Function send_200() has parameter $message with no type   
         specified.                                                
 ------ ---------------------------------------------------------- 
                                                       
 [ERROR] Found 146 errors
 ```

## JS linting

For finding problems in the Javascript code, I'm using [ESLint](https://eslint.org/), it analyzes the code without running and finds current ant potential errors. It's a dynamic tool, because everything is configurable. We can specify which files to ignore, what rules not to take in consideration. If the linting tool is installed globally, we can affect not only this project, but every other where Javascript is being used. The configuration file offers to be written in JavaScript, JSON, or YAML.

In this case, we want to target the file in src directory, because there is no need to analyze every single dependency file. In the terminal let's write this command:
```
npx eslint src/main.js
```
And if everything is fine we won't receive a response, otherwise errors will be listed to pay attention to.

## Avoid errors and enforce conventions in your styles

When working with SASS, it's easy to miss where styles are overlaying each other, or properties have been set in outdated or incorrect way. I am using [Stylelint](https://stylelint.io/) for this project. Similar as with other linters, this one has a configuration file called `.stylelintrc.json` in our project root. I have set the descending specificity to be ignored, because it's close to impossible to follow this rule when having large styling files. This tool stays up to date, and it's a neat tool to refresh the knowledge or learn something new when writing CSS styles.

To test our files, let's continue with this command:
```
npx stylelint "src/scss/*.scss"
```
It tests all the SCSS files in our directory and returns errors if any appear, otherwise no response is given in the terminal.
