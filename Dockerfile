FROM php:7.4-apache
WORKDIR /var/www/html
COPY ./src /var/www/html/
# composer vendor folder
COPY ./vendor /var/www/html/vendor

RUN apt-get update -y && apt-get install -y libmariadb-dev
# PDO connect to DB
RUN docker-php-ext-install pdo_mysql
# .htaccess allow
RUN a2enmod rewrite

# in most cases it will solve the issue with creating new files from script
#RUN chown -R www-data:www-data /var/www/html

# allows to create new files, but use 777 only for local development
RUN chmod -R 777 /var/www/html/
